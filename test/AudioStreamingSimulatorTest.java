import org.junit.*;
import java.util.*;
import play.Logger;
import play.test.*;
import play.libs.*;
import models.*;

public class AudioStreamingSimulatorTest extends UnitTest {

  @Test
  public void aVeryImportantThingToTest() {
    assertEquals(2, 1 + 1);
  }

  @Test
  public void testFetch() {
    try {
      AudioStreamingSimulator target = new AudioStreamingSimulator("piano1.ogg");
      long offset = 0;
      byte[] data = null;
      while ((data = target.fetch(offset)) != null) {
        offset += data.length;
        Logger.info(Codec.byteToHexString(data));
      }
      Logger.info("File size = %d", offset);
    } catch (Exception ex) {
      Logger.warn(ex, "Failed to testFetch");
      fail("file not found");
    }
  }
}