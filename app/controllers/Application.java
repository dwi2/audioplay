package controllers;

import play.*;
import play.mvc.*;
import play.libs.*;
import java.util.*;
import java.io.*;
import org.apache.commons.lang.*;

import models.*;

public class Application extends Controller {

  public static void index() {
    render();
  }

  public static void piano(String filename) {
    String fullname = filename + ".ogg";
    response.contentType = "audio/ogg";
    try {
      AudioStreamingSimulator source = new AudioStreamingSimulator(fullname);
      long offset = 0;
      byte[] data = null;
      while (source.hasMoreData(offset) > 0) {
        // not quite right
        data = source.fetch(offset);
        offset += data.length;
        response.writeChunk(data);
      }      
    } catch (FileNotFoundException ex) {
      notFound("File not found: " + fullname);
    } catch (Exception ex2) {
      notFound("Unknown errors: " + ex2.getMessage());
    }
  }
}