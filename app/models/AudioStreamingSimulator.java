package models;

import play.*;
import play.vfs.*;
import java.io.*;
import java.util.*;

public class AudioStreamingSimulator {

  public final static int BYTES_READ_PER_JOB = 100;

  private FileInputStream fs = null;
  private VirtualFile vf = null;

  public AudioStreamingSimulator(String filename) throws Exception {
    vf = VirtualFile.fromRelativePath("public/assets/"+filename);
    fs = new FileInputStream(vf.getRealFile());
    fs.close();
  }

  public byte[] fetch(long bytesToSkip) throws Exception {
    byte[] result = null;
    long available = 0;
    fs = new FileInputStream(vf.getRealFile());
    if (fs != null) {
      fs.skip(bytesToSkip);
      available = fs.available();
      if (available > 0) {
        if (available < BYTES_READ_PER_JOB) {
          result = new byte[Long.valueOf(available).intValue()];
        } else {
          result = new byte[BYTES_READ_PER_JOB];
        }
        fs.read(result);        
      } else {
        result = null;
      }
      fs.close();
    }
    return result;
  }

  public long hasMoreData(long bytesToSkip) {
    long available = 0;
    try {
      fs = new FileInputStream(vf.getRealFile());
      if (fs != null) {
        fs.skip(bytesToSkip);
        available = fs.available();
        fs.close();
      }
    } catch (Exception ex) {
      Logger.warn(ex, "Error occurs!");
    }    
    return available;
  }
}